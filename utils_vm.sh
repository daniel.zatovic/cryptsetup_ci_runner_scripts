#!/bin/bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/config.sh

get_vm_state() {
    VM_NAME="$1"
    virsh list --all | grep "$VM_NAME" | awk '{print $3}'
}

get_vm_ip() {
    VM_NAME="$1"
    virsh -q domifaddr $VM_NAME | head -n 1 | awk '{print $4}' | sed -E 's|/([0-9]+)?$||'
}

ssh_vm() {
    VM_IP="$1"

    ssh-keygen -R $VM_IP 2>&1 > /dev/null
    ssh $SSH_OPTS gitlab-runner@"$VM_IP"
}


wait_for_vm_ip() {
    VM_NAME="$1"

    echo 'Waiting for VM to get IP'
    for i in $(seq 1 $WAIT_SECONDS_IP_SSH); do
        VM_IP=$(get_vm_ip $VM_NAME)

        if [ -n "$VM_IP" ]; then
            echo "VM got IP: $VM_IP"
            return 0
        fi

        if [ "$i" == "$WAIT_SECONDS_IP_SSH" ]; then
            echo "Waited $WAIT_SECONDS_IP_SSH seconds for VM to start, exiting..."
            return 1
        fi

        sleep 1s
    done
}

wait_for_ssh() {
    VM_IP="$1"

    ssh-keygen -R $VM_IP 2>&1 > /dev/null
    echo "Waiting for sshd to be available"
    for i in $(seq 1 $WAIT_SECONDS_IP_SSH); do
        #if echo "exit" | ssh_vm $VM_IP >/dev/null 2>/dev/null; then
        if ssh $SSH_OPTS gitlab-runner@$VM_IP "exit" >/dev/null 2>/dev/null; then
            return 0
        fi

        if [ "$i" == "$WAIT_SECONDS_IP_SSH" ]; then
            echo "Waited $WAIT_SECONDS_IP_SSH seconds for sshd to start, exiting..."
            return 1
        fi

        sleep 1s
    done
}

wait_for_shutdown() {
    VM_NAME=$1
    
    echo "Waiting for VM to shutdown"
    for i in $(seq 1 $WAIT_SECONDS_IP_SSH); do
        if [ $(get_vm_state $VM_NAME) == "shut" ]; then
            return 0
        fi

        if [ "$i" == "$WAIT_SECONDS_IP_SSH" ]; then
            echo "Waited $WAIT_SECONDS_IP_SSH seconds for shutdown, exiting..."
            return 1
        fi

        sleep 1s
    done
}

test_vm_ssh() {
    VM_NAME="$1"

    wait_for_vm_ip $VM_NAME || return 1
    wait_for_ssh $(get_vm_ip $VM_NAME)
}

cloud_init_install() {
    DEVICE_PATH="$1"
    VM_NAME="${VM_DISTRO}-install"
    ARCH=${OS_ARCH:-x86_64}

    CLOUD_INIT_DATA=/tmp/cidata-${VM_DISTRO}-data
    mkdir $CLOUD_INIT_DATA
    cloud_init_meta_data > $CLOUD_INIT_DATA/meta-data
    cloud_init_user_data > $CLOUD_INIT_DATA/user-data

    CLOUD_INIT_ISO=/tmp/cidata-${VM_DISTRO}.iso
    rm -f $CLOUD_INIT_ISO
    genisoimage -output $CLOUD_INIT_ISO \
                -V cidata \
                -r \
                -J \
                $CLOUD_INIT_DATA/meta-data $CLOUD_INIT_DATA/user-data
    rm -rf $CLOUD_INIT_DATA

    virt-install \
        --name $VM_NAME \
        --disk path="$DEVICE_PATH",discard="unmap",io="threads",bus="scsi" \
        --controller type=scsi,model=virtio-scsi \
        --os-variant "$OS_VARIANT" --import \
        --vcpus=2 \
        --ram=$RAM_SIZE \
        --network network=$NETWORK_NAME \
        --graphics none \
        --arch $ARCH \
        --disk path=$CLOUD_INIT_ISO,device=cdrom
        #--noautoconsole \
        #--cloud-init meta-data=<(cloud_init_meta_data),user-data=<(cloud_init_user_data)

    wait_for_shutdown $VM_NAME || {
        virsh destroy $VM_NAME
        virsh undefine $VM_NAME
        rm $CLOUD_INIT_ISO
        return 1
    }

    virsh undefine $VM_NAME
    rm $CLOUD_INIT_ISO
}
