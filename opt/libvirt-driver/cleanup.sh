#!/usr/bin/env bash

# /opt/libvirt-driver/cleanup.sh

export VM_DISTRO=$1

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base script.
source "$CRYPTSETUP_CI_SCRIPTS_PATH/config.sh"
source "$CRYPTSETUP_CI_SCRIPTS_PATH/utils_vm.sh"

ARTIFACTS_NAME=${VM_ID}-artifacts
PART_NAME=$(kpartx "/dev/$VM_VG/$ARTIFACTS_NAME" | cut -d " " -f 1)

set -eo pipefail

virsh shutdown $VM_ID
wait_for_shutdown $VM_ID || {
    destroy_vm.sh $VM_ID
    exit
}
mkdir -p "/mnt/$VM_ID"
mount "/dev/$VM_VG/$ARTIFACTS_NAME" "/mnt/$VM_ID" 2>&1
cp /tmp/console-$VM_ID /mnt/$VM_ID
(cd "/mnt/" && gitlab-runner artifacts-uploader --expire-in "1 week" --url "https://gitlab.com" --token "$CUSTOM_ENV_CI_JOB_TOKEN" --id $CUSTOM_ENV_CI_JOB_ID --path "/mnt/$VM_ID" --artifact-format "zip" --artifact-type "archive") || true
umount "/mnt/$VM_ID"
rmdir "/mnt/$VM_ID"

destroy_vm.sh $VM_ID
