#!/usr/bin/env bash

# /opt/libvirt-driver/run.sh

export VM_DISTRO=$1
STAGE=$3

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base script.
source utils_vm.sh

if ([[ "$STAGE" == step_* ]] || [ "$STAGE" = "build_script" ]) && [[ $CUT_NETWORK != 0 ]]; then
    echo "Cutting network"
    MAC=$(virsh domiflist $VM_ID  | grep $NETWORK_NAME | awk '{print $5}')
    virsh detach-interface --domain $VM_ID --mac "$MAC" --type network || {
        echo "Failed to cut network"
        exit "$BUILD_FAILURE_EXIT_CODE"
    }
    echo "Network $MAC cut"
    sleep 5
fi

VM_IP=$(get_vm_ip $VM_ID)

ssh-keygen -R $VM_IP 2>&1 > /dev/null
ssh $SSH_OPTS gitlab-runner@"$VM_IP" /bin/bash < "${2}"
exit_code=$?
if [ $exit_code -eq 255 ]; then
    echo "Failed to ssh to VM (IP $VM_IP)"
    exit "$SYSTEM_FAILURE_EXIT_CODE"
fi
if [ $exit_code -ne 0 ]; then
    # Exit using the variable, to make the build as failure in GitLab CI.
    echo "Build script failed"
    exit "$BUILD_FAILURE_EXIT_CODE"
fi
exit 0
