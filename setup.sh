cat > gitlabci.xml <<EOF
<network connections='1'>
  <name>gitlabci</name>
  <forward mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
  </forward>
  <bridge name='virbr2' stp='on' delay='0'/>
  <ip address='192.168.42.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.42.2' end='192.168.42.254'/>
    </dhcp>
  </ip>
</network>
EOF

cat > isolated.xml <<EOF
<network connections='1'>
  <name>gitlabci</name>
  <bridge name='virbr3' stp='on' delay='0'/>
  <ip address='192.168.100.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.100.2' end='192.168.100.254'/>
    </dhcp>
  </ip>
</network>
EOF

virsh net-define gitlabci.xml
virsh net-start gitlabci
virsh net-autostart gitlabci

virsh net-define isolated.xml
virsh net-start isolated
virsh net-autostart isolated

pvcreate /dev/sda
vgcreate vg_data /dev/sda
lvcreate --thin -l 100%FREE vg_data/pool

dnf -y install libguestfs-tools-c gitlab-runner
dnf -y group install virtualization
dnf install -y tmux vim mc bash-completion gitlab-runner git libvirt-client qemu-kvm libvirt virt-top qemu-kvm qemu-guest-agent qemu-kvm-tools virt-install wget curl
virt-host-validate

systemctl enable --now libvirtd.service

ln -s /root/cryptsetup_ci_runner_scripts/opt/libvirt-driver/ /opt/libvirt-driver

# edit config.sh and opt/libvirt-driver/base.sh
# then run ./register_runner.sh <OS name> <Gitlab token>
