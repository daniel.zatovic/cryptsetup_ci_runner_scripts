# Cryptsetup CI Runner Scripts

Script for setting up Gitlab CI runner which uses libvirt to create a fresh VM
from snapshot for every pipeline job. Jobs in `.gitlab-ci.yml` have to be
marked with tags `libvirt` and distro name (currently `debian10` or `fedora34`)
to be picked up by the libvirt executor.

# Usage

## Installing base image:

`./install_base_image.sh <OS name>`

Current supported OSes: `centos-stream9`, `debian10`, `fedora34`, `fedora-rawhide`

This creates LV `gitlabci-<OS name>-0`

For every OS name, there must exist `os/<OS name>.sh` script, which has 2 functions
- `build_os_image` - 1 argument - path to block device where the base image should be installed
- `update_running_vm` - 1 argument - libvirt name of the VM to update

## Updating base image:

`./update_base_image.sh <OS name>`

This will create snapshot `gitlabci-<OS name>-x`, where x is the number of last
snapshot plus one. After the update, boot test is performed and if
unsuccessful, the snapshot is automatically deleted. To manually rollback to
previous snapshot, just delete the latest snapshot using `lvremove`.

## Manually creating VM from base image

`./create_vm_from_base_image.sh <OS name> <VM name> [<base snapshot name>]`

VM name is the name of the libvirt VM, latest base snapshot is used, you can
specify full name of older version.

After creating you can ssh into the VM using `./ssh_vm.sh <VM name>` and
destroy it using `./destroy_vm.sh <VM name>`.

Note: `./create_vm_from_base_image.sh` creates also the artifacts partition and
`./destroy_vm.sh` destroys the artifacts partition.
