#!/bin/bash

# virt-builder --list
VM_DISTRO="ubuntu-bionic"
# osinfo-query os
OS_VARIANT="ubuntu18.04"

OS_ARCH="i386"

source "$CRYPTSETUP_CI_SCRIPTS_PATH/config.sh"
source "$CRYPTSETUP_CI_SCRIPTS_PATH/utils_vm.sh"

cloud_init_meta_data() {
cat <<EOF
instance-id: ${VM_DISTRO}-runner
local-hostname: ${VM_DISTRO}-runner
EOF
}

cloud_init_user_data() {
cat <<EOF
#cloud-config
users:
  - name: gitlab-runner
    ssh_authorized_keys:
      - $(cat ${SSH_KEY}.pub)
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    groups: sudo
    shell: /bin/bash

debug: True

runcmd:
    - sudo cp /etc/apt/sources.list /etc/apt/sources.list~
    - sudo sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew update
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew upgrade
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew dist-upgrade
    - curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
    - curl -s "https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh" | bash
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew install debhelper
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew build-dep cryptsetup
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew install gitlab-runner git git-lfs openssh-server sudo $DEBIAN_PACKAGES
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew remove unattended-upgrades
    - sudo sed -i -e '/^APT::Periodic::\(Update-Package-Lists\|Unattended-Upgrade\)/s,1,0,' /etc/apt/apt.conf.d/20auto-upgrades
    - sudo bash -c "echo 'APT::Periodic::Enable \"0\";' >> /etc/apt/apt.conf.d/20auto-upgrades"
    - sudo systemctl disable apt-daily-upgrade.timer apt-daily.timer
    - runuser -l gitlab-runner -c "git lfs install --skip-repo"
    - echo "* - core unlimited" >> /etc/security/limits.conf
    - mkdir -p /var/coredumps/
    - chmod a+rwx /var/coredumps/
    - echo 'kernel.core_pattern = /var/coredumps/core.%e.%p.%h.%t' >> /etc/sysctl.conf

mounts:
 - [ LABEL=ARTIFACTS, /mnt/artifacts, "auto", "defaults,nofail,umask=000"]

power_state:
    mode: poweroff
    message: cloud-init finished
    timeout: $WAIT_MINUTES_INSTALL
    condition: True
write_files:
- path: /etc/netplan/0-config.yaml
  owner: root:root
  permissions: '0644'
  content: |
    network:
        ethernets:
            enss:
                dhcp4: true
                match:
                    name: ens*
            eths:
                dhcp4: true
                match:
                    name: eth*
        version: 2
EOF
}

build_os_image() {
    DEVICE_PATH="$1"

    UBUNTU_IMAGE="https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-i386.img"
    IMAGE_PATH="/tmp/bionic-server-cloudimg-i386.img"

    #curl -L $UBUNTU_IMAGE  -o $IMAGE_PATH
    qemu-img convert -f qcow2 -O raw $IMAGE_PATH $DEVICE_PATH

    cloud_init_install $DEVICE_PATH
}

update_running_vm() {
    VM_NAME=$1
    VM_IP=$(get_vm_ip $VM_NAME)

ssh $SSH_OPTS gitlab-runner@"$VM_IP" <<-\SSH
    export DEBIAN_FRONTEND=noninteractive
    apt_opts='-q -y -o Dpkg::Options::=--force-confnew'
    sudo -E apt-get $apt_opts update
    sudo -E apt-get $apt_opts upgrade
#END
SSH
}
