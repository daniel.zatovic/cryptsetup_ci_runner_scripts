#!/bin/bash

# virt-builder --list
VM_DISTRO="centos-stream9"
# osinfo-query os
OS_VARIANT="centos-stream9"

source "$CRYPTSETUP_CI_SCRIPTS_PATH/config.sh"
source "$CRYPTSETUP_CI_SCRIPTS_PATH/utils_vm.sh"

cloud_init_meta_data() {
cat <<EOF
instance-id: ${VM_DISTRO}-runner
local-hostname: ${VM_DISTRO}-runner
EOF
}

cloud_init_user_data() {
cat <<EOF
#cloud-config
users:
  - name: gitlab-runner
    ssh_authorized_keys:
      - $(cat ${SSH_KEY}.pub)
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    groups: sudo
    shell: /bin/bash
debug: True

runcmd:
    - curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | os=el dist=8 bash
    - curl -s "https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh" | os=el dist=8 bash
    - dnf --verbose -y --best config-manager --set-enabled crb
    - dnf --verbose -y --best install gitlab-runner git git-lfs openssh-server sudo $CENTOS_PACKAGES
    - gem install asciidoctor
    - runuser -l gitlab-runner -c "git lfs install --skip-repo"
    - echo "* - core unlimited" >> /etc/security/limits.conf
    - mkdir -p /var/coredumps/
    - chmod a+rwx /var/coredumps/
    - echo 'kernel.core_pattern = /var/coredumps/core.%e.%p.%h.%t' >> /etc/sysctl.conf

mounts:
 - [ LABEL=ARTIFACTS, /mnt/artifacts, "auto", "defaults,nofail,umask=000"]

power_state:
  mode: poweroff
  message: cloud-init finished
  timeout: $WAIT_MINUTES_INSTALL
  condition: True
EOF
}

build_os_image() {
    DEVICE_PATH="$1"

    CENTOS_IMAGE_SOURCE="https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20211208.0.x86_64.qcow2"

    #wget -O /tmp/CentOS-Stream-GenericCloud-9.x86_64.qcow2 $CENTOS_IMAGE_SOURCE
    qemu-img convert -f qcow2 -O raw /tmp/CentOS-Stream-GenericCloud-9.x86_64.qcow2 $DEVICE_PATH

    cloud_init_install $DEVICE_PATH
}

update_running_vm() {
    VM_NAME=$1
    VM_IP=$(get_vm_ip $VM_NAME)

ssh $SSH_OPTS gitlab-runner@"$VM_IP" <<-\SSH
    sudo dnf --verbose -y --best update
#END
SSH
}
