#!/bin/bash

# virt-builder --list
VM_DISTRO="fedora-rawhide"
# osinfo-query os
OS_VARIANT="fedora-rawhide"

source "$CRYPTSETUP_CI_SCRIPTS_PATH/config.sh"
source "$CRYPTSETUP_CI_SCRIPTS_PATH/utils_vm.sh"

SYSTEMD_PACKAGES="swtpm meson ninja-build python3-jinja2 gperf libcap-devel tpm2-tss-devel libmount-devel swtpm-tools"

cloud_init_meta_data() {
cat <<EOF
instance-id: ${VM_DISTRO}-runner
local-hostname: ${VM_DISTRO}-runner
EOF
}

cloud_init_user_data() {
cat <<EOF
#cloud-config
users:
  - name: gitlab-runner
    ssh_authorized_keys:
      - $(cat ${SSH_KEY}.pub)
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    groups: sudo
    shell: /bin/bash
debug: True

runcmd:
  - dnf -y --best upgrade
  - curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | os=fedora dist=33 bash
  - curl -s "https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh" | os=fedora dist=33 bash
  - dnf --verbose -y --best install gitlab-runner git git-lfs openssh-server sudo $FEDORA_PACKAGES $SYSTEMD_PACKAGES
  - runuser -l gitlab-runner -c "git lfs install --skip-repo"
  - dnf -y install linux-firmware
  - dnf -y install kernel --disablerepo=* --enablerepo="fedora_rawhide_kernel_nodebug" --skip-broken
  - echo "* - core unlimited" >> /etc/security/limits.conf
  - mkdir -p /var/coredumps/
  - chmod a+rwx /var/coredumps/
  - echo 'kernel.core_pattern = /var/coredumps/core.%e.%p.%h.%t' >> /etc/sysctl.conf

mounts:
 - [ LABEL=ARTIFACTS, /mnt/artifacts, "auto", "defaults,nofail,umask=000"]

power_state:
  mode: poweroff
  message: cloud-init finished
  timeout: $WAIT_MINUTES_INSTALL
  condition: True

yum_repos:
  fedora-rawhide-kernel-nodebug:
    name: Rawhide kernels built without debugging turned on
    baseurl: https://alt.fedoraproject.org/pub/alt/rawhide-kernel-nodebug/\$basearch
    enabled: true
    #skip_if_unavailable: true
    gpgcheck: false

  fedora-rawhide-kernel-nodebug-debuginfo:
    name: Rawhide kernels built without debugging turned on - Debug
    baseurl: https://alt.fedoraproject.org/pub/alt/rawhide-kernel-nodebug/debuginfo
    enabled: false
    #skip_if_unavailable: true
    gpgcheck: false

write_files:
- path: /etc/sysconfig/network-scripts/ifcfg-eth0
  owner: root:root
  permissions: '0775'
  content: |
    BOOTPROTO=dhcp
    DEVICE=eth0
    ONBOOT=yes
    TYPE=Ethernet
    USERCTL=no
- path: /etc/sysconfig/network-scripts/ifcfg-eth1
  owner: root:root
  permissions: '0775'
  content: |
    BOOTPROTO=dhcp
    DEVICE=eth1
    ONBOOT=yes
    TYPE=Ethernet
    USERCTL=no
EOF
}

build_os_image() {
    DEVICE_PATH="$1"

    LATEST=$(curl -L https://dl.fedoraproject.org/pub/fedora/imagelist-fedora 2> /dev/null | grep Cloud | grep '.raw.xz' | grep 'Rawhide' | grep 'x86_64')
    RAWHIDE_IMAGE_SOURCE="https://dl.fedoraproject.org/pub/fedora/$LATEST"
    curl -L $RAWHIDE_IMAGE_SOURCE | xzcat -q | dd of=$DEVICE_PATH status=progress conv=sync,noerror

    cloud_init_install $DEVICE_PATH
}

update_running_vm() {
    VM_NAME=$1
    VM_IP=$(get_vm_ip $VM_NAME)

ssh $SSH_OPTS gitlab-runner@"$VM_IP" <<-\SSH
    sudo dnf upgrade -y --exclude kernel,kernel-modules
    sudo dnf upgrade -y --disablerepo=* --enablerepo="fedora_rawhide_kernel_nodebug" --skip-broken
#END
SSH
}
