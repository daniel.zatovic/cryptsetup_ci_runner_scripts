#!/bin/bash

# virt-builder --list
VM_DISTRO="debian-11"
# osinfo-query os
OS_VARIANT="debian11"

source "$CRYPTSETUP_CI_SCRIPTS_PATH/config.sh"
source "$CRYPTSETUP_CI_SCRIPTS_PATH/utils_vm.sh"

SYSTEMD_PACKAGES= "swtpm meson ninja-build python3-jinja2 gperf libcap-dev tpm2-tss-engine-dev libmount-dev swtpm-tools pkgconf"

cloud_init_meta_data() {
cat <<EOF
instance-id: ${VM_DISTRO}-runner
local-hostname: ${VM_DISTRO}-runner
EOF
}

cloud_init_user_data() {
cat <<EOF
#cloud-config
users:
  - name: gitlab-runner
    ssh_authorized_keys:
      - $(cat ${SSH_KEY}.pub)
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    groups: sudo
    shell: /bin/bash
debug: True

runcmd:
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew update
    - curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
    - curl -s "https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh" | bash
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew install -t bullseye-backports debhelper
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew build-dep cryptsetup
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew install gitlab-runner git git-lfs openssh-server sudo $DEBIAN_PACKAGES $SYSTEMD_PACKAGES
    - DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::=--force-confnew remove unattended-upgrades
    - sudo sed -i -e '/^APT::Periodic::\(Update-Package-Lists\|Unattended-Upgrade\)/s,1,0,' /etc/apt/apt.conf.d/20auto-upgrades
    - sudo bash -c "echo 'APT::Periodic::Enable \"0\";' >> /etc/apt/apt.conf.d/20auto-upgrades"
    - sudo systemctl disable apt-daily-upgrade.timer apt-daily.timer
    - runuser -l gitlab-runner -c "git lfs install --skip-repo"
    - echo "* - core unlimited" >> /etc/security/limits.conf
    - mkdir -p /var/coredumps/
    - chmod a+rwx /var/coredumps/
    - echo 'kernel.core_pattern = /var/coredumps/core.%e.%p.%h.%t' >> /etc/sysctl.conf

mounts:
 - [ LABEL=ARTIFACTS, /mnt/artifacts, "auto", "defaults,nofail,umask=000"]

power_state:
  mode: poweroff
  message: cloud-init finished
  timeout: $WAIT_MINUTES_INSTALL
  condition: True
EOF
}

build_os_image() {
    DEVICE_PATH="$1"

    DEBIAN_11_IMAGE="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.raw"
    curl -L $DEBIAN_11_IMAGE | dd of=$DEVICE_PATH status=progress

    cloud_init_install $DEVICE_PATH
}

update_running_vm() {
    VM_NAME=$1
    VM_IP=$(get_vm_ip $VM_NAME)

ssh $SSH_OPTS gitlab-runner@"$VM_IP" <<-\SSH
    export DEBIAN_FRONTEND=noninteractive
    apt_opts='-q -y -o Dpkg::Options::=--force-confnew'
    sudo -E apt-get $apt_opts update
    sudo -E apt-get $apt_opts upgrade
#END
SSH
}
