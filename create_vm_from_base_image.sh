#!/bin/sh

if [[ ! $# -eq 2 ]] && [[ ! $# -eq 3 ]]; then
    echo "Usage: $0 <OS name> <VM name> [<base snapshot name>]"
    exit 1
fi
OS_NAME="$1"
VM_NAME="$2"

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/config.sh

if [ ! -f "$CRYPTSETUP_CI_SCRIPTS_PATH/os/${OS_NAME}.sh" ]; then
    echo "OS $OS_NAME is not supported"
    exit 1
fi

source "$CRYPTSETUP_CI_SCRIPTS_PATH/os/${OS_NAME}.sh"
source "$CRYPTSETUP_CI_SCRIPTS_PATH/utils_vm.sh"

DISK_SIZE=${OS_DISK_SIZE:-$DISK_SIZE}
RAM_SIZE=${OS_RAM_SIZE:-$RAM_SIZE}
ARCH=${OS_ARCH:-x86_64}

VM_DISK=$VM_IMAGES_PATH/$VM_NAME
ARTIFACTS_NAME=${VM_NAME}-artifacts
ARTIFACTS_DISK=$VM_IMAGES_PATH/$ARTIFACTS_NAME

if [[ $# -eq 3 ]] ; then
    VM_BASE_LV=$3
    VM_BASE_IMAGE="$VM_IMAGES_PATH/$VM_BASE_LV"
else
    LAST_UPDATE_NUMBER=$(lvs $VM_VG -o name | grep -E "${VM_BASE_LV%-*}-[0-9]" | rev | cut -d'-' -f1 | rev | sort -n -r | head -n 1)
    LAST_UPDATE_NUMBER=$((LAST_UPDATE_NUMBER))
    LAST_SNAPSHOT_NAME="${VM_BASE_LV%-*}-$LAST_UPDATE_NUMBER"

    if [ ! -e /dev/$VM_VG/$LAST_SNAPSHOT_NAME ]; then
        echo "Base image $LAST_SNAPSHOT_NAME doesn't exist"
        exit 1
    fi

    VM_BASE_LV=$LAST_SNAPSHOT_NAME
    VM_BASE_IMAGE="$VM_IMAGES_PATH/$VM_BASE_LV"
fi

echo "Creating snapshot $VM_NAME from $VM_VG/$VM_BASE_LV"

lvcreate -s $VM_VG/$VM_BASE_LV -n $VM_NAME -kn >/dev/null 2>/dev/null || {
    echo "Failed to create LVM snapshot"
    exit 1
}

echo "Creating VM $VM_NAME with $OS_NAME and disk on $VM_DISK"

lvcreate -V $ARTIFACTS_SIZE --thin -n $ARTIFACTS_NAME $VM_VG/$THIN_POOL || exit 1

mkfs.vfat -F32 /dev/$VM_VG/$ARTIFACTS_NAME -n ARTIFACTS

virt-install \
    --name "$VM_NAME" \
    --disk path="$VM_DISK",discard="unmap",cache="unsafe",io="threads",bus="virtio" \
    --os-variant "$OS_VARIANT" --import \
    --vcpus=2 \
    --ram=$RAM_SIZE \
    --network network=$NETWORK_NAME \
    --network network=$ISOLATED_NETWORK_NAME \
    --graphics none \
    --disk /dev/$VM_VG/$ARTIFACTS_NAME \
    --serial file,path=/tmp/console-$VM_NAME \
    --arch $ARCH \
    --noautoconsole

test_vm_ssh $VM_NAME || {
    echo "Virtual machine does not boot. Cleaning up"
    destroy_vm.sh "$VM_NAME"
    exit 1
}
