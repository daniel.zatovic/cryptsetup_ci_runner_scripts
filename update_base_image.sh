#!/bin/sh

if [[ ! $# -eq 1 ]] && [[ ! $# -eq 2 ]]; then
    echo "Usage: $0 <OS name> [<base snapshot name>]"
    exit 1
fi

OS_NAME="$1"

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/config.sh

if [ ! -f "$CRYPTSETUP_CI_SCRIPTS_PATH/os/${OS_NAME}.sh" ]; then
    echo "OS $OS_NAME is not supported"
    exit 1
fi

source "$CRYPTSETUP_CI_SCRIPTS_PATH/os/${OS_NAME}.sh"
source "$CRYPTSETUP_CI_SCRIPTS_PATH/utils_vm.sh"

LAST_UPDATE_NUMBER=$(lvs $VM_VG -o name | grep -E "${VM_BASE_LV%-*}-[0-9]" | rev | cut -d'-' -f1 | rev | sort -n -r | head -n 1)
LAST_UPDATE_NUMBER=$((LAST_UPDATE_NUMBER))
VM_UPDATE_LV="${VM_BASE_LV%-*}-update"

if [[ $# -eq 2 ]] ; then
    VM_BASE_LV=$2
else
    VM_BASE_LV="${VM_BASE_LV%-*}-$LAST_UPDATE_NUMBER"
fi

VM_BASE_IMAGE="$VM_IMAGES_PATH/$VM_BASE_LV"

lvs $VM_BASE_IMAGE &>/dev/null || {
    echo "Cannot update because base image ($VM_BASE_IMAGE) doesn't exist"
    exit 1
}

VM_DISK="$VM_IMAGES_PATH/$VM_UPDATE_LV"
VM_NAME=$VM_UPDATE_LV

create_vm_from_base_image.sh $OS_NAME $VM_NAME $VM_BASE_LV

echo "Installing with disk $VM_DISK"

update_running_vm $VM_NAME || {
    echo "Failed to update VM image"
    destroy_vm.sh "$VM_NAME"
    exit 1
}

ssh $SSH_OPTS gitlab-runner@"$VM_IP" <<-\SSH
    sudo fstrim / || true
#END
SSH

virsh shutdown $VM_NAME

wait_for_shutdown $VM_NAME || {
    echo "Failed to shutdown update VM"
    destroy_vm.sh "$VM_NAME"
    exit 1
}

virsh undefine $VM_NAME

ARTIFACTS_NAME=$VM_NAME-artifacts
ARTIFACTS_DISK=$VM_IMAGES_PATH/$ARTIFACTS_NAME
if [ -b "$ARTIFACTS_DISK" ]; then
    umount -f $ARTIFACTS_DISK
    lvremove -y $VM_VG/$ARTIFACTS_NAME
fi

TEST_VM_NAME="${VM_BASE_LV}-boot-test"

create_vm_from_base_image.sh $OS_NAME $TEST_VM_NAME $VM_UPDATE_LV || {
    lvremove -y $VM_VG/$VM_UPDATE_LV
    echo "The virtual machine doesn't boot after update. Update snapshot destroyed."
    exit 1
}

destroy_vm.sh $TEST_VM_NAME
echo "Update successful moving snapshot"

TO_DELETE=$((LAST_UPDATE_NUMBER - $KEEP_SNAPSHOTS + 1))

if [[ $TO_DELETE -ge 0 ]]; then
    lvremove -y "$VM_VG/${VM_BASE_LV%-*}-$((TO_DELETE))"
fi

lvrename -y $VM_VG $VM_UPDATE_LV "${VM_BASE_LV%-*}-$((LAST_UPDATE_NUMBER + 1))"

exit 0
