#!/bin/bash

THIN_POOL=pool
VM_VG=vg_data

VM_BASE_LV=gitlabci-$VM_DISTRO-0
VM_IMAGES_PATH="/dev/$VM_VG"
VM_BASE_IMAGE="$VM_IMAGES_PATH/$VM_BASE_LV"
RUNNER_INSTALL_PATH="/opt/libvirt-driver"

ARTIFACTS_SIZE=1G
DISK_SIZE=10G
RAM_SIZE=2048
KEEP_SNAPSHOTS=5

NETWORK_NAME="gitlabci"
ISOLATED_NETWORK_NAME="isolated"
CUT_NETWORK=0

SSH_KEY="/root/.ssh/id_rsa"
SSH_OPTS="-i $SSH_KEY -o StrictHostKeyChecking=no"
WAIT_SECONDS_IP_SSH=30
WAIT_MINUTES_INSTALL=10

CENTOS_PACKAGES="autoconf automake device-mapper-devel gcc gettext-devel json-c-devel
                  libblkid-devel libpwquality-devel libselinux-devel
                  libssh-devel libtool libuuid-devel make popt-devel
                  libsepol-devel nc openssh-clients passwd pkgconfig sharutils
                  sshpass tar uuid-devel vim-common device-mapper expect gettext git jq
                  keyutils openssl-devel openssl gem"

FEDORA_PACKAGES="autoconf automake device-mapper-devel gcc gettext-devel json-c-devel
                  libargon2-devel libblkid-devel libpwquality-devel libselinux-devel
                  libssh-devel libtool libuuid-devel make popt-devel
                  libsepol-devel netcat openssh-clients passwd pkgconfig sharutils
                  sshpass tar uuid-devel vim-common device-mapper expect gettext git jq
                  keyutils openssl-devel openssl asciidoctor"

DEBIAN_PACKAGES="git gcc make autoconf automake autopoint pkgconf libtool
                libtool-bin gettext libssl-dev libdevmapper-dev libpopt-dev uuid-dev
                libsepol1-dev libjson-c-dev libssh-dev libblkid-dev tar libargon2-0-dev
                libpwquality-dev sharutils dmsetup jq xxd expect keyutils
                netcat passwd openssh-client sshpass asciidoctor"

export CRYPTSETUP_CI_SCRIPTS_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export PATH="$CRYPTSETUP_CI_SCRIPTS_PATH:$PATH"
